## how to set up:

```bash
npm install
```

## How to run

```bash
npx grunt serve
```

## Help on RevealJS

* See project page at https://github.com/hakimel/reveal.js/
* We are using Markdown syntax
  * Renders slides from slides.md in root
  * Two linefeeds = a new slide
  * Three linefeeds _should_ equal a new section (horizontal)
  * Use vim arrows to navigate up/down (jk)
  * Add notes for yourself with Note: on a line
  * Use three-tick + language for code samples
  * Look at slides.md for a sample


