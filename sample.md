## Agenda

* What is JavaScript
* History of JavaScript
* Language Features
* JavaScript and the DOM
* Jasmine and testing


## What is JavaScript?

* A programming language developed by NetScape
    * Created in 10 days by Brendan Eich
    * Originally named Mocha by Marc Andreessen
    * Changed to LiveScript, then JavaScript

* JavaScript started as a language for browsers
    * Capitalized on the emerging Java name from Sun
    * But could not copywright the name JavaScript
    * Named ECMAScript after working with ECMA to create a language
      specification for others to use


## JavaScript runs everywhere

* Browsers
* Servers (with NodeJS)
* Mobile phones
* Some "Internet of Things" devices


## Language Features

* Dynamically typed variables
    * Several primitive data types (string, number, boolean)
    * Arrays, Objects, Functions are the base of all other variables
* Programming styles focus on
    * Functional programming style
    * Object support
* Sloppy default coding style
* "Secret" strict mode provides more checks


## Warm-up Exercise - 1.1.1

* Fire up application server
    * Run Chrome, open up developer tools
    * Switch to JavaScript Console

* Experiment with
    * Variables and types
    * Creating simple functions
    * The `console.dir and console.log statements`


## JavaScript Versions

* ES3 (< 2005)
* ES4 (DEAD! Didn't ship)
* ES5 (2008+)
* ES6/2015 (now)
* ES7/2017 (future)

_We're looking at ES5, since it is supported by a vast majority of browsers today_

*JavaScript is always backward-compatible*


## Basic language features

* Variables
* Functions
* Objects
* Variable scoping to enclosing functions (or global)
* Built-in functions and objects
* Interacts with HTML via the DOM


## Creating variables with `var`

* Variables are defined with the `var` keyword
* *OR* automatically defined when initialized as "global"

## Using var - samples

Any variable without `var` is defined globally (window)

```javascript
// a global variable (no 'var' prefix)
a = 12;    // this is VERY bad

console.log('a is', a);
// or
console.log('a is' , window.a);
```


## Always use `var` to define a variable properly

```javascript
// created in current context
var a = 12;

// assign multiple variables
var a = 12, b = 13;

// create variable names but leave 'undefined'
var a, b;
```


## You can re-assign to other values, even different types

```javascript
var a = 12;
a = 'hi there';
```

* In JavaScript, types are dynamically assigned
* You can use the `typeof` operator to ask what type is currently assigned

```javascript
var a = 12;
console.log(typeof a); // "number"
```


## JavaScript types - primitives and objects

* In JavaScript there are only three primitives
    * `number` - any numeric value
    * `string` - any sequence of characters
    * `boolean` - a true/false conditional
* Everything else is an `object` or `function`
    * This includes `arrays`, which act as objects
    * There are also object "wrappers" -`String`, `Number` and `Boolean`


## Primitive Types


## The JavaScript `number` type

* Double-precision 64-bit binary format IEEE 754 values
    * Stores numbers between (-2^53-1 and 2^53-1)
    * That is a huge range
    * You can capture between 15 to 17 significant digits in a floating point number
* That's confusing, so...  Here are legal number expressions
    * 0, 1, 2, 3, -1, -100010 or any typical "integer"
    * 0.234324324324, -24324234.23 or any typical floating point number
    * 2e13 - numbers in scientific notation representing 20000000000000
    * 0x99 - Hexidecimal (decimal 153)


## Creating numbers

* Use the `var` keyword and a valid number format

```javascript
var a = 234.54;
var b = 234.555 / 310.1;
var c = 3 * 24^2;
```

## Other sources of numbers

* The `Math` object - utility functions, constants
* Math constant such as `Math.PI`, `Math.E`, `Number.MAX_VALUE`
* Parse a number from a String, ex: parseInt('12')

```javascript
var mathOfAges = Math.PI / Math.E;

var cos3 = Math.cos(3);
```

## There is a Number utility object

* Provides methods for numeric instances dynamically at runtime
    * `myNumber.toPrecision(4)` - truncates the digits of precision of the number attached to is
    * `myNumber.toFixed(4)` - convert to a fixed point string format of n significant digits for display, truncating after n digits
    * `myNumber.toPrecision(4)` - convert to a fixed-point string format for display, only using n digits - may convert to scientific notation

```javascript
var x = 23424234.333;
x.toFixed(1);       // 23424234.3
x.toPrecision(4);   // 2.342e+7
x.toString();       // 23424234.333;
```


## Parsing numbers

You can parse using the built-in methods `parseInt` and `parseFloat` on `window`

* These don't need a prefix
* But in later JavaScript versions, they use `Number.` as their prefix

```javascript

// ES5, 5.1 syntax
var n = parseInt('234234');   // 234234
var n2 = parseFloat('3/2');   // 3, ignores past number
var n3 = parseFloat('3.323'); // 3.323
var n4 = parseFloat('bad');   // NaN
```


## What is NaN?

In JavaScript, an invalid number is set as `NaN`

* This is an actual IEEE number, but not a valid one
* There are more than one of these (imagine divide by zero, unparseable)
* You can't just compare to NaN
* JavaScript has the `isNaN()` function to check for that

```javascript
console.log(isNaN(parseFloat('bad'));
// true (more on booleans later...)
```


## Constructor Functions and Objects - Number

* Another feature of the `Number` object - it's also a `constructor function`
* The `Number` constructor creates a wrapped Number object

```javascript
var n = new Number(1200);

x.valueOf();   // primitive 12
x.toPrecision(2);  // 1.2e+3
```

## Auto-wrapping primitive

This happens automatically with primitives, so use them normally

```javascript
var n = 1200;
// n is dynamically wrapped in Number, then
// `toPrecision` is called
n.toPrecision(2);  // 1.2e+3
```

Otherwise `Number` is useful for utility functions and constants


## Why only one number type?  C has tons!

Mistakes are made...

* Wrong precison is a big source of errors (too few digits = lack of accuracy)
* Wrong range is another big source of errors (eg: byte = < 255)
* You _will_ have rounding issues for certain operations

```javascript
var twoThirds = 2/3;     // 0.666666666666666
twothirds.toPrecision(3) // 0.667 (rounds up)
// but this is odd
0.1 + 0.2   // 0.30000000000000004
```

See [What Every JavaScript Developer Should Know About Floating Points](https://modernweb.com/what-every-javascript-developer-should-know-about-floating-points/)


## JavaScript and Math...

* Be careful - precision errors are very small but magnify when repeated
    * You can use another API if sanctioned such as `mathjs` or `BigDecimalJs`
    * You can do all calculations on monetary values on a Java-based server
      and return JSON data to the client
    * You can calculate in high-precision floating point and lop off the end
      when outputting strings using `toPrecision()` or `toFixed()`


```javascript
// Strings can use single or double quotes
// but single is preferred, Concat with +

var a = 'this is my ' + ' life ';
```


## Pre-excercise prep - Jasmine

* We're going to use Jasmine, a testing API, to learn JavaScript
* Jasmine is a function-based testing library
* Some of what you'll see we explain later (function definitions, etc)
* But you'll be able to write your experiment code in the tests and run it


## A Jasmine Spec

```javascript
(function() {
  'use strict';

  describe('a number is a number', function() {
     it('should set a number properly', function() {
        var a = 12;
        expect(a).toBe(12);
     });
  });
}());
```

* `describe` defines a specification (set of tests)
* `it` defines a given test
* The `use strict` and wrapping function provide a strict coding mode


## Running the tests - the Karma runner

* To run the test runner, issue this command line in the labs directory
```javascript
run-tests lab-01-start
```

* Now edit the file `language-basics-spec.js` in `test/spec/lab-01-start`
  and follow along with the instructor. Each time you save the file, it will re-run the configured tests.


## Excercise Time!

* Work together on exercises 1.1.1 - 1.1.5


## Break


## Variables and types

* All types are dynamically assigned
* Use `typeof` operator to return the type

```javascript
var x = 123;
typeof x
> "number"

typeof 1234
> "number"
typeof '2134124'

> "string"

typeof true
> "boolean"
```


## Default value for unassigned variables - `undefined`

Takes the place of traditional `NULL` in JavaScript

```javascript
var x

typeof x
> "undefined"
```


## There is also the `null` keyword

Means `explicitly no value`

```javascript

var x = null;
typeof x;    "object"   // yes, "null" is an object
```

## Key distinction - `undefined` is _not_ `null`

* You _can_ assign `null` to the variable if you want

```javascript
var x = null, y;

typeof x === typeof y
> false
```

* This is a conscious choice - `null` and `undefined` are different
* `null` is just a special object


## The `string` type

* A primitive (unlike Java)
* A sequence of characters
* Surrounded by quotes (single or double, single preferred)
* May be concatenated with the `+` operator

```javascript
var x = 'abc123';
var y = x + 'def456';
console.log(y);     // 'abc123def456'
console.log(y[1]);  // 'b'
```


## The `String` object contains extra helper functions

* These are automatically provided to primitive strings too

```javascript
// equivalent operations
console.log(new String('uppercase me').toUpperCase());

var x = 'uppercase me';
console.log(x.toUpperCase());
```


## String lengths and concatenation

* The `string` type has a _read-only_ `length` property
* You can concatenate strings with the `.concat` operator, this works in
  other sequences such as arrays too

```javascript
var mystr = 'abc'.concat('def');
console.log(mystr.length);  // 6
```


## `boolean` - true/false values

* `true` and `false` are literal symbols
* `Boolean` object wraps and converts

```typescript
var x = true;
var y = false;
```


## JavaScript and "truth"

* There are several ways to generate a boolean value beyond straight
  assignment
    * Comparison using `==`
    * Comparison using `===`
    * Check against a reference for _"truthiness"_


## JavaScript boolean evaluation using `==`

* The `==` check stands for _convert, then compare_
    * JavaScript will attempt to make two variables comparable
    * Once they can be compared, it performs a check
    * This implies a conversion
* There are some very bizarre rules built into JavaScript's `truthiness`
  algorithms


## Conditionals and booleans with `==`

* JavaScript supports typical `if... else...` comparison constructs

```javascript
var a = 23;
if (a == 23) {
  console.log('a is 23!');
}
```

### Truthiness with `==`

With `==`, this also is a true condition

```javascript
var x = 23;
if (x == '23') {
   console.log('x ');
}
```

* Remember, `==` is convert and compare
* This can get quite complex - see http://dorey.github.io/JavaScript-Equality-Table/


## Special JavaScript conversion rules

Compares to `true`

* any number except `0`
* `true`
* an object
* an array with at least one element...

Compares to false

* `0`
* An empty array
* `null`, an undefined variable


## Using truthiness with a variable

```javascript
if (a) {
  console.log('a is truthy');
}
```

* follows truthiness rules (i.e. has to be a defined object, array with
  at least one value, not `null` or undefined, or a defined primitive
* Useful when using presence of something to continue
* But only when used with the variable. Don't use `==` or `===` in this
  form


## JavaScript prefers the  `===` comparator

* `A === B` - must EXACTLY equal (no conversions)

    * `2 === '2'` No

    * `true === 1` No

* ALWAYS USE `===` unless you have an absolutely valid reason to use `==`

* Hint there are basically none except the truthy object existence
  comparison we just saw


## Exercise 1.1.6 - 1.1.10


## Break


## Functions

* Functions declare callable code blocks
    * Can return objects, values or functions
    * Can accept objects, values or functions as arguments
    * May define their own local variables

```javascript
function add(a, b) {
    return a + b;
}

var result = add(3, 5);
```


## Functions have their own type

```javascript
var adder = function(a, b) {
  return a + b;
};

typeof adder
> "function"
```

* Function is similar to object - it can have properties and even other
  methods attached to it. But for now, we'll focus on the fact that it
  is a named block of code with inputs and outputs


## Variables inside of functions

Variables defined within a function live within the function only

```javascript

function add(a, b) {
  var result = a + b;
  console.log('result is', result);
  return result;
}

var value = add(3, 4);
console.log('result is', value);  // valid
console.log('result is', result);  // invalid
```

* `result` thrown away when the function call ends


## Function reference variables

* Function creation statements return a function variable reference

```javascript
var addfn = function add(a, b) {
   return a + b;
};

// call it two ways:
add(1, 3);
addfn(a, b);
```

* We can use this to pass the function to another method


## Shorthand - create function as variable

* You may drop the function name and make the function itself
  anonymous
* It is just an anonymous function referenced by a variable

```javascript
var add = function(a, b) {
...
}

var c = add(1, 4);
```


## Functional programming

Functions can be passed into and returned from other functions

```javascript
var calcExec = function(operation, a, b) {
  return  operation(a, b);
};

var result = calcExec(addfn, 1, 3);
```


## Polymorphism and JavaScript
Dynamic typing and lack of classes helps JavaScript handle polymorphism easily

```javascript
calcExec(addFn, 1, 34);
calcExec(subtractFn, 1, 54);
```

* Your methods just have to take two parameters and return a result

* In Java - you'd need an interface or base class, implementation classes, and an operation method signature


## Nesting functions

* The inner function can see all variables in the outer function

* The outer function can call the inner function where needed

```javascript
function processOrder(orderNum, price, qty) {

  function calcSalesTax(taxRate) {
    return price * qty * taxRate;
  }

  var total = calcSalesTax(0.06) + price * qty;
  // do other work
  return total;
}

processOrder(23423, 10, 150.30);
```


## Nesting with function reference variables

* The `var` approach or the function name approach are just two styles to create a function reference

```javascript
var processOrder = function (orderNum,
                             price, qty) {

  var calcSalesTax = function(taxRate) {
    return price * qty * taxRate;
  }

  var total = calcSalesTax(0.06) + price * qty;
  // do other work
  return total;

}
```

## Exercises 1.1.11 - 1.1.13


## Break


## JavaScript is object-based

* You define objects in JavaScript using curly braces `{` and `}`
* Objects do not have class definitions (until JS 1.6)
* Objects can contain properties
* Properties can be
    * Objects
    * Functions
    * Variables


## Creating Objects

Using

* JavaScript Literals
* Constructor Functions
* Other functions


## JavaScript Literal Syntax

A JavaScript Literal object definition

```javascript
var customer = {
  firstName: 'Ken',
  lastName: 'Rimple',
  age: 1500,
  dateOfBirth: new Date('01/15/1400')
};
```

* Create a JavaScript object in-line with a literal syntax
* Very popular for one-off objects


## Constructor functions

* Configure an object (`this`) by adding properties
* Allow for initialization logic
* Uses the `new` keyword, and the `this` variable allows for attaching functions and properties to the new object

```javascript
var Customer = function(firstName, lastName) {
  this.firstName = firstName;
  this.lastName = lastName;

};

// to use, ALWAYS use the new keyword:
var customer1 = new Customer('Joe', 'Smith'),
    customer2 = new Customer('Sally', 'Jones');
```

## Object properties

Properties are

* Defined either at creation time or later, by attaching properties using one of two methods
    * The `.` property notation as in `foo.bar`
    * The indexed property notation as in `foo['bar']`
* Each has their uses...


## Direct property access with `'.'`

```javascript
// access firstName
customer.firstName     // 'Ken'

// modify firstName
customer.firstName = 'Sal';

// add a property
customer.middleInitial = 'J';
```

## Indexed property access

* JavaScript properties can be accessed via string-based keys
* JavaScript literals can then act as indexed collections
* Useful when key is a longer string (with spaces, illegal variable name characters etc)...

```javascript
// access /modify firstName
customer['firstName']  // 'Ken'
customer['firstName'] = 'Joe';

// use a var (programmatic access)
var propName = 'firstName'
customer[propName] = 'Fred'

// invalid native property - contains spaces
// but works for APIs with keys like maps
customer['sales orders'] = 23423;
```


## Typeof and objects

There is just one `object` type

```javascript

typeof {}
> "object"

typeof { a: "a", b: "b" }
> "object"

typeof []
> "object"
```

## Detecting constructors of objects

For objects created with `new`, or by language constructs that use constructor-like logic, we can infer the type by its constructor

```javascript
var myFoo = new Foo(...);
myFoo.constructor === Foo;     // true

var myStr = "hi there, I am a string";
myStr.constructor === String;  // true

var array = [1, 4, 5];  // implicit new Array
array.constructor === Array;  // true
```

_This is not always valid - you can change many object type constructors (except Number, String, Array, etc) at runtime..._


## Special comparisons for objects

```javascript
// is something defined?
typeof item !== 'undefined'

// better
item !== undefined or
!item  // truthy (not undefined or null)

// is something an array?
typeof item === 'object' &&
   typeof item.length !== 'undefined'

// better
Array.isArray(item)   // ES5.1
```


## Variable scope and Context

* Variable visibility
* The `this` context variable


## JavaScript visibility rules

* Function variables are visible to any inner functions
* Object variables are visible to any other properties of the object
* Object variables can be accessed outside using either object syntax or index syntax

```javascript
var globalA = 234;
var dataSet = {
  x: 23423,
  label: 'asfdasdf',
  print: function() {
    console.log(globalA, this['x'], this.label);
  }
};
```


## There are three variable storage locations to be aware of

* Global (window)
* Function or nested function
* Object property

* Your variable lives in a particular location depending on where it is
  defined


## Global variables

* As we've stated, any variable defined without `var` keyword is global
* In browsers, global = `window`
* However, any unwrapped variable definition is also global:

```html
   <script>
     var x = 234;  // or window.x = 234;
   </script>

   <script>
     console.log('x is', x);
     // same as
     console.log('x is', window.x);
   </script>
```


## Object functions and execution

* Execute functions by using `objectName.functionName(params)` syntax

```javascript
// given
var myCustomer = {
  firstName: 'Ken',
  lastName: 'Rimple',
  getFullName : function() {
    return this.firstName + ' ' + this.lastName;
  }
};

// to execute
myCustomer.getFullName();   // 'Ken Rimple'
```


## Contexts

* JavaScript has a `this` symbol
* The `this` symbol represents the current _context_
* It is either `window` or an object
* Which it is assigned to can be a bit confusing


## The `this` keyword in JavaScript

Mostly depends on the way it is called

```javascript
var x = myFunction(y);  // this = window

var x = y.myFunction(z);  // this = y

{
  a: 'b',
  b: function() {
     this.a = '234324'; // this = the object
  }
}

// z is the new object (this in constructor)
var z = new MyConstructor();
```


## `this` may change during callbacks

See chapter on `this` in _JavaScript, The Good Parts_

```javascript
var customerManager = {
  customers: [], // a list

  get: function() {
     return this.customers;  // right!
  },

  load: function() {
     setTimeout((function() {
        this.customers = [];  // wrong!
     }, 2000);
  }
};
```


## To be safe with callbacks

Alias `this` to another variable at the beginning, like `self`

```javascript
var customerManager = {
  customers: [], // a list

  get: function() {
     return this.customers;
  },

  load: function() {
     var self = this;   // self will not change
     doSomething().then(function(data) {
        self.customers = data;  // Correct!
     });
  }
};
```


## Understanding `this` assignment rules

* `this` is set by analyzing the calling expression
   * does it contain a dot? `this` is the object to the left of the dot
   * otherwise `this` is `window`
   * unless the call registers a callback that is executed later...
     Then `this` might be something else...
* In that case, save off `this` to a function variable to keep it from changing


## By reference -vs- by value

* All objects and functions are passed by reference
    * Assigning a variable will make it reference the
      object or function
    * Arrays are objects (sort of)
* All primitives are passed by value
    * Assigning a variable to a primitive makes a copy of it


## Reference and Value examples

```javascript
var a = 234;
var b = a;    // copy - by value

var a = function() { ... };
var b = a;    // both point to the same function

var a = [1, 3, 4];
var b = a;    // both point to the array

var a = { b: '3' };
var b = a;    // both point to the same object
```


## Variable guidelines - Do

* Always declare variables with the `var` keyword

  * pass variables into functions

  * refer to the object member functions live in with `this` if needed

  * consider aliasing `this` to another name to avoid async callback issues

  * Use JSLint or JSHint to help find problem areas in your code


## Variable guidelines - Do not

* rely on global variables
    * unless they are utility libraries or frameworks (jQuery, Angular, Underscore)
* forget to use the `var` keyword when defining variables


## Cloning (copying) an Object

Just assigning a new `var` only creates a new reference:

```javascript
var obj = obj2;  // they both point to the same object
```

There is no built-in clone feature in ES5, so we have to make our own:

```javascript
var objectCopy = { };
for (var key in objectOriginal) {
  if (objectOriginal.hasOwnProperty(key)) {
    objectCopy[key] = objectOriginal[key];
  }
}
```

_AngularJS provides `angular.copy(src, dest)` helper_


## Exercises - 1.1.14 to 1.1.20


## Break


## Arrays - Ordered sequence of items

Assign a variable to a comma-separated list

* or empty list with Square brackets `[ ]`,
* or `new Array()`

```javascript

var myArray = [2, 43, 'hi'];
var myEmptyArray = [];
var myArrayViaObjct = new Array(10);
```


## Elements accessors

Use square brackets `[ ]`

```javascript
myArray[0] = 1234;
myArray[23] = { ... };
```


## Walking an array

Use the `length` property and a loop

```javascript
for (var i = 0; i < ary.length; i++) {
  console.log('val is', ary[i]);
}
```

Or use the `for ... in` approach

```javascript
for (var elementIdx in ary) {
  console.log('val is' , ary[elementIdx]);
}
```

The latter is preferred


## Helpful Array Methods

* `unshift(element)` - add an element to the front of the array
* `shift()` - return and remove the first element in the array
* `push(element)` - add an element to the end of the array
* `pop()` - return and remove an element from the end of the array
* `slice(a, b)` - returns elements starting at `a` and ending before `b` (b omitted - rest of array)
* `sort()` - sorts by unicode values (!) - in place!
* `sort(comparatorFn)` - sort by values outputted by comparator equation - in place!
* `concat(ary2)` - join the current array with elements from the second array to return a new array


## Examples

```javascript
var beatles = ['John', 'Paul',
               'George', 'Ringo', 'Pete', 'Stu'];

beatles.push('George Martin');  // add honorary Beatle

var earlyBeatles =
    beatles.slice(0, 2).concat(beatles.slice(4));
// 'John', 'Paul', 'Pete', 'Stu'

var beatlesCSV = beatles.join(', ');

beatles.sort();  // sorts in-place alphabetically
```


## Dates
* The bane of all programmer existence
* JavaScript uses the `Date` class to construct
* Dates are milliseconds since Jan 1, 1970 UTC (epoch)
* This value is contained as `valueOf()`

```javascript
var earlyDate = new Date(1901, 12, 15);
earlyDate.valueOf();  // -2144689200000

var lateDate = new Date(2013, 01, 15);
lateDate.valueOf();   // 1360904400000

// date objects can be compared
lateDate > earlyDate  // true
```


## Date object functions

* Creating a new date based on the current time - `var now = new Date()`
* Current time in millis (from epoch) - `var nowMS = Date.now()`
* There are other constructors:

```javascript
var
  // epoch is Jan 1, 1970
  d1 = new Date(104234244),
  d2 =
    new Date('January 1, 1971 12:31:15 PM'),
  // month starts from zero, not 1
  d3 =
    new Date(2013, 1, 15, 12, 15, 13, 1033);
```


## Exercises 1.1.21 - 1.1.29

## *Key JavaScript built-in objects

* Math
* Object


## `Math` - Mathematical features

* Upper and lower bounds for integers, floats
* Standard constants such as `E` and `PI`
* Functions such as randomization, rounding, logarithms, exponential functions, sin, cos, etc.

```javascript
Math.PI
Math.random()
Math.sin(0.3)
```


##  `Object` - can be used to create objects and manipulate them

* `Object.create(null|object)` - create a new object based on `object` as a prototype
* `Object.freeze(var)` - prohibits modifying or adding properties - `unfreeze` reverses this
* `Object.seal(var)` - prohibits adding new properties to an object, but current property values can change - see `unseal` to release
* `Object.eval(str)` - execute JavaScript and return a result - dangerous if misused


## *Extending objects

Objects do not have classical inheritence as in Java or C

* But JavaScript instead uses the concept of a dynamically assignable _prototype_

You can assign this prototype with two techniques

* Define a `prototype` function of your constructor to assign the same instance to all created objects
* Dynamically create a new object using `Object.create(prototype)`


## Using the `prototype` property of a constructor

Attaching prototype to a constructor:

```javascript
var Person = function(first, last) {
  this.first = first;
  this.last = last;
};

Person.prototype = {
  getName: function() {
    return this.first + " " + this.last;
  }
};

var p = new Person('Ken', 'Rimple');
p.getName(); // Ken Rimple
```


## Use `Object.create` to make p2 based on p1

Here you can programmatically assign the prototype

```javascript
var proto = {
  getName: function() {
  return this.first + ' ' + this.last;
  }
};

var p = Object.create(proto);
p.first = 'Joe'; p.last = 'Smith';
p.getName(); // 'Joe Smith'
```

The constructor way is preferred as it is automatic


## An Object's prototype - key points

* A prototype is an object that can be used to add additional methods and data to an object
    * Assigned during `new` or `Object.create`
    * There is _no default prototype_ (undefined by default)
    * If attached, JavaScript searches upwards from the object to each parent prototype for methods or properties before it gives up
    * A property found in a prototype will return false from `myObj.hasOwnProperty(prop)`


## JavaScript and Page Load

* JavaScript is executed *as it's seen in document*
    * May pause loading
    * DOM may not be fully constructed
    * Hard to maintain scripts mixed with markup
* `load` even indicates that DOM has loaded, is ready to run


## Typical JS Literal Object

* Create an object to hold state, functions

* Use `this` to refer to other members within the object

```javascript
var calculator = {
  paperTape : [],
  add : function(a, b) {
    var result = a + b;

    this.paperTape.push(
      'a + b = ' + result);
    return result;
  },
  subtract : ...
  ...
}
```


## Method Invocation
```javascript
// invoke method function
calculator.add(1, 4);
// show state
console.log(calculator.paperTape);
```
* The object `calculator` has state

* `this` represents the calculator instance itself

* Problem: we directly expose the paperTape

* JavaScript passes objects / arrays by reference - this can be modified!

* Solution: JavaScript closure, also AngularJS has a nice clone function - `angular.copy`


## Let's use closure to hide paperTape
* JavaScript allows variables declared just oustide of a function to be captured

```javascript
function createCalculator() {
     var paperTape = [];  // captured
  return {
     add : function(a, b) {
        var result = a + b;
        paperTape.push('a + b = ' + result);
        return result;
      },
     getTape : function() {
        return angular.copy(paperTape);
     }, ..
  };
}
```


## Closure key points

* Any variable or function defined just outside of your new object - captured by closure property of JavaScript

    * Avoids using `this` for state variables

    * Makes it easier to determine what to expose or hide

    * These objects act like hidden member variables

    * A key component for good JS designs

* Remember to watch JavaScript's `by reference` passing of variables, otherwise you leak them anyway


## Exercises 1.30 - 1.32


## JavaScript and the Browser

Document Object Model (DOM)

* The _DOM_ is the API for accessing the content on the page
    * provided by the window's `document` object
    * `document.getElementById()` - finds an element to manipulate by its HTML element ID
    * `document.createElement('tag')` - creates a new element
* The DOM is loaded asynchronously
    * Event is fired once DOM is stable `DOMContentLoaded`
    * You can manipulate the DOM after this event


## DOM Example - Adding a list item to a list

Given

```html
<ul id="list">
</ul>
```

Use

```javascript
document.addEventListener("DOMContentLoaded",
  function(event) {
    var theList = document.getElementById('list');
    var newItem = document.createElement('li');
    newItem.innerHTML = '<b>Wow, this was easy(?)</b>';
    theList.addChild(newItem);
});
```


## JavaScript Object Notation (JSON)

* The default language between JavaScript and the outside world
* Uses the anonymous block concept
    * Objects are surrounded with { .. }
    * Arrays surrounded with [ ]
    * Comma-separated properties
    * Any functions are removed
* Modern browsers support with the JSON object
    * `JSON.stringify(obj)` - turn an object/array into a string
    * `JSON.parse(str)` - turn a string into an object/array


## JSON <-> JavaScript

```javascript
var customer = {
  firstName: 'Ken',
  lastName: 'Rimple'
};
var cstring = JSON.stringify(customer);
// '{"firstName":"Ken","lastName":"Rimple"}'

var cust2 = JSON.parse(cstring);
cust2.firstName   // 'Ken'
```


## Uses of JSON
* Configuration settings
* Local storage in your browser
* Ajax data format
* Database storage format
* Alternative - BSON (Binary JSON - used by MongoDb)

## Control of Flow

* If... else
* while
* do... while
* switch


## The conditional statement - if

Full syntax:

```javascript
if (condition) {
  // code if true
} else if (condition2) {
  // code if true
} else {
  // code if not any of above
}
```

* You _can_ use non-braced predicates, but shouldn't
* You _can_ skip the `else if` or `else` conditions


## Implicit conditional - assignment

Assign this, unless something is false:

```javascript
// create an empty array if sourceArray is not initialized
var myData = sourceArray || [];

// even run code if something isn't assigned
sourceArray || initializeSystem();
```


## `for` loops...

A way to iterate a number of times or through the elements of an array or object

```javascript
for (var i = 0; i < 100; i++) {
  console.log('iteration', i);
}

for (var idx in arry) {
  console.log('arry[' + idx + '] is', arry[idx]);
}

for (var prop in window) {
  console.log('window.' + prop + 'is', window[prop]);
}
```


## `while` and `do...while`

Alternative looping structures

```javascript
// test first
while (i < 100) {
  i++;
  console.log('i is', i);
}

// test at the end
do {
  i++
} while (i < 100);
```

## The `forever until condition loops`

```javascript
// variant - forever until
while (true) {
  ...
  if (skippingCondition) { continue; }
  if (endingCondition) { break; }
}
```

* Should always have a way out
* Otherwise you'll need some way to sleep. Very hard in JS without callbacks.


## Switch statements

Can make a choice between a set of different discreet values

```javascript
switch(account.category) {
  case 1:
    account.accountType = 'Checking';
    break;
  case 2:
    account.accountType = 'Savings';
    break;
  // no break = fall through and
  // run block under next condition  case 3:
  case 4:
    account.accountType = 'Brokerage';
    break;
  default:
    account.accountType = 'Invalid type';
};
```


## Wrap-up

* JavaScript is a powerful language
* Its idioms are significantly different from Java
* Functional, dynamic programming models are common


## Lab 1 - Functional JavaScript
* Bank Account Lab
* Polymorph Calculator lab

