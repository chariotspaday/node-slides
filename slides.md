# Shakras and Hot Stone Therapy (SHST)

### Modern techniques for relaxation using ancient methods

### (a hands-on approach)



# Server Architectures with Node

### Why should the browser folk have all the fun?

Note:

* entry-level talk for those most familiar with client side
* Hope some new tidbits for experienced node developers
* There are no puns in this talk. I `promise`.



# Your friendly presenter


Jeffrey Labonski

jlabonski@chariotsolutions.com

Consultant, Chariot Solutions

Somewhat Recent Javascript Convert

Note: Name, address, telephone number



# Javascript Convert?

### Actually, yes <!-- .element: class="fragment" -->


Note:
I've been a "static typed, compile always" guy forever.

Sure, JS has flaws

* Written in two weeks like you forgot about the exam
* Named after Java because Java is "cool"
* Been described as: 'An example of mass psychosis'
* Doesn't even have odd numbers

We can overlook some of the flaws, 'cause the rest rocks



# However, there's beauty in there

### This may take a bit of time to find. <!-- .element: class="fragment" -->


---


# Efficient use of resources makes me happy.



# This thing is _thin_



Customer production server from `pm2`:

```
$ pm2 status

│ SERVER (PROD) │ online | 12D    │ 0%  │ 148.7 MB |
```

# 12 days of uptime <!-- .element: class="fragment" -->
# 150MB of RAM! <!-- .element: class="fragment" -->


Note:

* Got 8 of these running in 1.5G of RAM
* Makes those AWS nano and micro instances look mighty attractive
* No BS, it processes payments, generates PDFs, and speaks SOAP to legacy
    services



`java -Xmx4G -Xms4G -jar app.jar`

```
 :: Spring Boot ::        (v2.0.3.RELEASE)

Started TestApp in 24.351 seconds (JVM running for 24.811)
```


Note: node startup in a handful of millis



# Single threaded


Note:

* Less to worry about
* No shared memory issues, no synchronization contexts
* Just run a bunch of them, up to num(CPU)
* Crash? Run another one.



# Code isomorphism between client and server

### If you use JS on the client...



# JS is a gateway drug to Lambda


---


# Node architecture makes me happy
(hapi?) <!-- .element: class="fragment" -->


Note: I want to spend some time talking about the _how_ and _why_ of node

* internals
* how to leverage
* don't make my mistakes



# Node was born from two neat pieces of technology

### `libuv` <!-- .element: class="fragment" -->

### `v8` <!-- .element: class="fragment" -->



![Put the two together...](images/reeses.jpeg)



![Node Architecture](images/nodejsarch.svg)


Note:

* Javascript is the icing
* Multilayer core of "things that do work"



# `v8` & `shakra`


Note:
Javascript is a fun language as it can't actually do any work

* No inherent IO, can't communicate to the world
* About as useful as BASIC
* In the browser, it's welded to the DOM
* On the server, it's kinda pointless unless embedded as a scripting language
    (Nashorn, JDK6)



# `libuv`


Note: nonblocking, continuation driven IO

* Nonblocking- saves CPU
* With CPU free, it can do _more_ IO
* Evolved from "libeio" and "libev"



# Only a matter of time
![Put the two together...](images/reeses.jpeg)


Note:

* Really only a matter of time, _something_ was gonna happen\
* JS has no baggage, no SDKs that could interfere with the style of UV, of
    deferred IO.
* Not having any standard library to speak of makes the marriage pretty easy
* Right place, right time
    * browsers used to, well, do everything
    * Javacript programmers abound
    * Javascript built for event handling (callbacks)



# What, exactly, is it good for?



# IO

# <!-- .element: class="fragment" --> Like, _tons_ of IO



![Node Architecture](images/nodejsarch_evhigh.svg)


Note:

* All work is C++
* Single threaded, if you're doing work you're not processing requests or
  responding to events
* Waiting for anything goes to the queue and allows node to process again
* Event Queue is adaptive, round-robin, and auto-adjusts



# Try not to think too much

### All CPU use on node is crimethink
### Get in, get out



# A humorous vingette...


Note:

* Did a load test against prod release
* spun up a few thousand users, had them all start bashing on the server
* One hotspot in the profiler



# `bcrypt(3)`*!!!!!*



# WARNING:

# Handrails are not available

Note: You MUST leverage what node is good at. Don't do complex calculations,
render templates, etc, etc.



# There really is only one real method in node that matters.


# `on()` <!-- .element: class="fragment" -->



![EventEmmitter.on](images/on_docs.png)



# Looks familiar?

```javascript
server.on('request', (req, res) => {
    // node HTTP server
})

observer.subscribe((x) => {
    // Angular HttpClient
})
```



# The fundamental unit of javascript is the _continuation_.
# Javascript is a _continuation oriented_ language.


Note:

* Javascript is not a OO language
* It's not really a functional language
* Yes, it is prototypal
* But (IMHO) it's _continuation oriented_



```javascript
whenItIsDone(thing, (result) => {
    doSomethingElse(result)
})
```

Note: When something is done, do something else with the result



```javascript
const fs = require('fs')

fs.readFile('./foo.txt', 'utf8', (err, contents) => {
    console.log(contents)
});
```



# The _continuation_ is a lambda

# It executes later. <!-- .element: class="fragment" -->

# You can never get here from there.<!-- .element: class="fragment" -->



```javascript
                                 (err, contents) => {
    console.log(contents)
});
```



```javascript
fs.readFile('./foo.txt', 'utf8', (err, contents) => {
    console.log(contents)
});

fs.readFile('./bar.txt', 'utf8', (err, contents) => {
    console.log(contents)
});
```



# This is the beauty of nodejs

# Handle your result *later*  <!-- .element: class="fragment" -->


---


# Man, it can get ugly fast



```javascript
const fs = require('fs')
const dns = require('net')

fs.readFile('./foo.txt', 'utf8', (err, contents) => {
    fs.writeFile('./bar.txt', 'utf8', (err, contents) => {
        dns.lookup(contents, (err, address, family) => {
            console.log(address)
        })
    })
})
```



```javascript
do(foo, bar => {
    some(bar, baz => {
        thing(baz, quux => {
            complex(quux, quuz => {
                eventually(quuz, waldo => {
                    go(waldo, fred => {
                        crazy(fred, thud => {
                            console.log(thud)
                        })
                    })
                })
            })
        })
    })
})
```

Note:

* And I'm using 4 space tabs, like a heretic



# Promises are syntactic sugar



```javascript
do(foo)
    .then(bar => {
        some(bar)
    }).then(baz => {
        thing(baz)
    }).then(quux => {
        complex(quux)
    }).then(quuz => {
        eventually(quuz)
    }).then(waldo => {
        go(waldo)
    }).then(fred => {
        crazy(fred)
    }).then(thud => {
        console.log(thud)
    }).catch(err => {
        // whoops
    })
```



### `async` / `await` are the new hotness



```javascript
try{
    const bar   = await do(foo)
    const baz   = await some(bar)
    const quux  = await thing(baz)
    const quuz  = await complex(quux)
    const waldo = await eventually(quuz)
    const fred  = await go(waldo)
    const thud  = await crazy(fred)
    console.log(thud)
}catch(err){
    // whoops
}
```



```javascript
const util = require('util')

const readFileAsync = util.promisify(fs.readFile)

const contents = await readFileAsync('./foo.txt', 'utf8')
```



![http://node.green](images/node.green.png)


Note: node.green



![http://node.green](images/node.trimleft.png)



```javascript
const f = (a, b) => {
    return a + b
}

const p = f('a', ?)

assert(p('b') === 'ab')
```


Note: stage 1 proposal, but still...

---


# Let's do some actual _work_.



# REST servers

* Express
* HAPI
* Restify
* Loopback
* Sails
* ... probably a baker's dozen more by now



# Express.js is the gold standard

### Does whatcha need



```javascript
const express = require('express')
const app = express()
const port = 80

app.get('/', (req, res) => res.send('Hello World!'))

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
```



# C'mon get HAPI


Note:



# git@bitbucket.org:chariotspaday/server.git



# Things to like

### In this desolate wasteland of `~0.0.9`


Note:

* Heavily documented
* Really, really well documented
* Running prod at Walmart, Conde Nast
* 100% code coverage in test
* _Transition_ documents for upgrading versions
* Very well thought out functionality
* Good plugin support



# Vibrant plugins, easy API to write your own

* good
* boom
* nes
* poop
* bell
* blipp



```javascript
server.route({
    method: 'GET',
    path: '/',
    handler: function (request, h) {
        return 'Hello!'
    }
})
```

Note: Routing is simple, just a decorated event handler

* hapi 17 is callback free, all async from end to end


# hapi ❤️ joi

```javascript
const session = joi.object().keys({
    id: joi.number().integer().min(1).required(),
    name: joi.string().trim(true).min(1).required(),
    date: joi.string().trim(true).isoDate().required()
})
```


Note:

joi is this interesting library that makes sure data adheres to a schema. It's
much more potent than jsonschema, extendable, and gives excellent and verbose
error messages.



```javascript
method: 'GET',
options: {
    tags: ['api'],
    description: 'Gets all subscriptions for a session',
    path: '/session/{id}/subscriptions',
    validate: {
        params: {
            id: joi.number().integer().min(1)
        }
    },
```


Note: joi is tightly integrated with hapi, and you can ensure that all user
input conforms to required structure. Including optional, and/or/xor/etc, when
this is greater than that, this field is required, etc.



```javascript
response: {
        options: {
            abortEarly: false
        },
        sample: 100,
        schema: joi.array().items(joiSchemas.registration)
    }
```


Note: but it's even better, 'cause you can enforce your own constraints on
outgoing



```json
"/api/session": {
    "get": {
        "responses": {
            "default": {
                "description": "",
                "schema": {
                    "type": "array",
                    "items": {
                        "$ref": "#/definitions/IdNameDateModel"
                    }
                }
            }
        },
        "produces": ["application/json"],
        "tags": ["api"],
        "summary": "Gets all sessions"
    }
},
```


Note: generats swagger (with a plugin)



![Autogenerated documents](images/server_docs.png)



# Stupid node tricks with hapi, express, and websockets



```javascript
const hapi = require('hapi')
const ws = require('ws')

const server = new hapi.Server({port: 8000})
const listener = server.listener

const wss = new WebSocket.Server({
    server: listener,
    path: '/api/chat'
})

await server.start()
```



```javascript
this._removeListeners = addListeners(this._server, {
    listening: this.emit.bind(this, 'listening'),
    error: this.emit.bind(this, 'error'),
    upgrade: (req, socket, head) => {
        this.handleUpgrade(req, socket, head, (ws) => {
        this.emit('connection', ws, req);
        });
    }
});
```



```javascript
server.events.on('log', (event, tags) => {
server.events.on('request', (request, event, tags) => {
server.events.on('response', (request) => {
```


---


# Takeaway (whew!)

* Understand `on()`
* Grok continuations, how code moves through node
* Never eat CPU.
* Use cool language features
* Be Javascripty on the server
